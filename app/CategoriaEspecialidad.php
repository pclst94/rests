<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaEspecialidad extends Model 
{

    protected $table = 'categoriaEspecialidad';
    protected $fillable = ['nombre', 'idCatPadre'];
    public $timestamps = true;

    public function getCategoriasHijo()
    {
        return $this->hasMany('App\CategoriaEspecialidad', 'idCatPadre', 'id');
    }

    public function getCategoriaPadre()
    {
        return $this->belongsTo('App\CategoriaEspecialidad', 'idCatPadre');
    }

    public function getRestaurantes()
    {
        return $this->hasMany('App\Restaurante');
    }

}