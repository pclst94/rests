<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\TipoProducto as TipoProductoResource;
use App\Http\Resources\Sucursal as SucursalResource;

class Restaurante extends Model 
{

    protected $table = 'restaurante';
    protected $guarded = [];
    public $timestamps = true;

    public function getTiposproductos()
    {
        $tiprods = $this->hasMany('App\TipoProducto','idRestaurante')->get();
        $collection = collect([]);
        foreach($tiprods as $tp){
            $tpex = new TipoProductoResource($tp);
            $collection = $collection->concat([$tpex]);
        }
        return $collection;
    }

    public function getSucursales()
    {
        $sucs = $this->hasMany('App\Sucursal', 'idRestaurante')->get();
        $collection = collect([]);
        foreach($sucs as $sc){
            $scr = new SucursalResource($sc);
            $collection = $collection->concat([$scr]);
        }
        return $collection;
    }

    public function getComentariosRating()
    {
        return $this->hasMany('App\ComentariosRating', 'idRestaurante');
    }

    public function getCategoriaEspecialidad()
    {
        return $this->belongsToMany('App\CategoriaEspecialidad');
    }

    public function getHorario()
    {
        return $this->belongsTo('App\Horarios', 'idHorario');
    }

}