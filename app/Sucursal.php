<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model 
{

    protected $table = 'sucursal';
    protected $fillable = ['idRestaurante', 'idDistrito', 'nombreSucursal', 'direccion', 'telefono', 'coordenadas'];
    public $timestamps = true;

    public function getRestaurante()
    {
        return $this->belongsTo('App\Restaurante', 'idRestaurante');
    }

}