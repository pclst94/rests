<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distrito extends Model 
{

    protected $table = 'distrito';
    public $timestamps = true;
    protected $fillable = ['idProvincia','nombre'];

    public function getSucursales()
    {
        return $this->hasMany('App\Sucursal');
    }

}