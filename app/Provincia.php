<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model 
{

    protected $table = 'provincia';
    public $timestamps = true;
    protected $fillable = ['nombre'];

    public function getDistritos()
    {
        return $this->hasMany('App\Distrito', 'idProvincia', 'id');
    }

}