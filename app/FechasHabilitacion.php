<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FechasHabilitacion extends Model 
{

    protected $table = 'fechasHabilitacion';
    protected $fillable = ['fechaInicio', 'fechaFinal'];
    public $timestamps = true;

}