<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Http\Resources\ProductoExtra as ProductoExtraResource;
use Illuminate\Support\Facades\Log;

class TipoProducto extends Model 
{

    protected $table = 'tipoProducto';
    protected $fillable = ['idRestaurante','nombre','isExtra'];
    public $timestamps = true;

    public function getProductos()
    {
        setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
        date_default_timezone_set('America/Bogota');
        $current_day = strftime("%A");        
        $prods = $this->hasMany('App\ProductoExtra','idTipoDeProducto')->get();
        $collection = collect([]);
        foreach($prods as $p){
            Log::debug("trayendo: " . $p);
            if($p->idProductoPadre != null) continue;
            $service_hours = $p->getHorario()->get();
            $service_date_begin = $p->getFechasHabilitacion()->get();
            //si tiene fechas de habilitación
            if( count($service_date_begin) > 0 ){
                
                $date_end = $service_date_begin[0]["fechaFinal"];
                $date_begin = $service_date_begin[0]["fechaInicio"];
                // ver si el producto está dentro de la fecha de habilitación
                if( date('Y-m-d') >= $date_begin && date('Y-m-d') <= $date_end ){
                    // ver si tiene horas de disponibilidad
                    if(count($service_hours) > 0){
                        $current_service_hours = $service_hours[0][$current_day];
                        if (
                          date('H:i') > substr($current_service_hours,0,strpos($current_service_hours,";")) &&
                          date('H:i') < substr($current_service_hours,strpos($current_service_hours,";") + 1)
                        ) {
                            $pex = new ProductoExtraResource($p);
                            $collection = $collection->concat([$pex]);
                        }
                    }
                    // tiene fecha de habilitación pero no tiene horario de disponibilidad,
                    // entonces se lista porque su restaurante ya verificó la hora de atención
                    else{
                        $pex = new ProductoExtraResource($p);
                        $collection = $collection->concat([$pex]);
                    }
                }
            }
            //no tiene fechas de habilitación
            else{
                // pero tiene horario de disponibilidad
                if(count($service_hours) > 0){
                    $current_service_hours = $service_hours[0][$current_day];
                    //verifica si está dentro de ese horario
                    if (
                    date('H:i') > substr($current_service_hours,0,strpos($current_service_hours,";")) &&
                    date('H:i') < substr($current_service_hours,strpos($current_service_hours,";") + 1)
                    ) {
                        $pex = new ProductoExtraResource($p);
                        $collection = $collection->concat([$pex]);
                    }
                }
                // no tiene horario de disponibilidad ni fechas de habilitación,
                // simplemente se lista porque su restaurante ya se encarga de verificar un horario
                else{
                    $pex = new ProductoExtraResource($p);
                    $collection = $collection->concat([$pex]);
                }
            }
            
            //Log::debug("pex: ".$pex);
        }
        return $collection;
    }

}