<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiasFestivos extends Model 
{

    protected $table = 'diasFestivos';
    protected $fillable = ['fecha'];
    public $timestamps = true;

    public function getHorarios()
    {
        return $this->hasMany('App\Horarios','idDiafestivo');
    }

}