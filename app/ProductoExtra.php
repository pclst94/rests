<?php

namespace App;
ini_set("memory_limit","512M");

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\ProductoExtra as ProductoExtraResource;
use App\Http\Resources\ProductVariation as ProductVariationResource;
use Illuminate\Support\Facades\Log;

class ProductoExtra extends Model 
{

    protected $table = 'productoExtra';
    public $timestamps = true;
    protected $fillable = ['idProductoPadre', 'idHorario', 'idTipoDeProducto', 'titulo', 'descripcion', 'imagen', 'horariosFechasApertura', 'limiteExtra', 'isOfferOrProduct', 'isExclusiveOffer', 'idFechasHabilitacion'];

    public function getTipoProducto()
    {
        return $this->belongsTo('App\TipoProducto', 'idTipoDeProducto');
    }

    public function getProductoPadre()
    {
        return $this->belongsTo('App\ProductoExtra', 'idProductoPadre');
    }

    public function getAcompanamientos()
    {
        // return a collection of productoextra
        $extras = $this->hasMany('App\ProductoExtra', 'idProductoPadre')->get();
        $collection = collect([]);
        foreach($extras as $ex){
            Log::debug("tiene: " . $ex);
            if($ex->idVariacion) continue;
            $pex = new ProductoExtraResource($ex);
            $collection = $collection->concat([$pex]);
        }
        return $collection;
    }

    public function getVariations()
    {
        $vars = $this->hasMany('App\ProductVariation','idProductoExtra')->get();
        $collection = collect([]);
        foreach($vars as $v){
            $vr = new ProductVariationResource($v);
            $collection = $collection->concat([$vr]);
        }
        return $collection;
    }

    public function getHorario()
    {
        return $this->hasOne('App\Horarios', 'id', 'idHorario');
    }

    public function getFechasHabilitacion()
    {
        return $this->hasOne('App\FechasHabilitacion','id', 'idFechasHabilitacion');
    }

}