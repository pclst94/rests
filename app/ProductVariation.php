<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\ProductoExtra as ProductoExtraResource;
use App\Http\Resources\ProductVariation as ProductVariationResource;

class ProductVariation extends Model 
{

    protected $table = 'productVariation';
    protected $fillable = ['titulo','idProductoExtra','idVariacion','precio','sku'];
    public $timestamps = true;

    // This is to get the parent i guess
    public function getProductExtra()
    {
        return $this->belongsTo('App\ProductoExtra', 'idProductoExtra');
    }

    //get 
    public function getExtras()
    {
        $extras = $this->hasOne('App\ProductoExtra', 'idVariacion')->get();
        $varsextras = $this->hasOne('App\ProductVariation', 'idVariacion')->get();
        $collection = collect([]);
        foreach($extras as $ex){
            $pex = new ProductoExtraResource($ex);
            $collection = $collection->concat([$pex]);
        }
        foreach($varsextras as $vex){
            $vpex = new ProductVariationResource($vex);
            $collection = $collection->concat([$vpex]);
        }
        return $collection;
    }

}