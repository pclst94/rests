<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Restaurante extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tiposDeProducto' => $this->getTiposproductos(),
            'sucursales' => $this->getSucursales(),
            'razonSocial' => $this->razonSocial,
            'Ruc' => $this->Ruc,
            'Telefono' => $this->Telefono,
            'montoMinimoPedido' => $this->montoMinimoPedido,
            'ratioDeliveryCostoKilometro' => $this->ratioDeliveryCostoKilometro,
            'costoMinimoDelivery' => $this->costoMinimoDelivery,
            'direccion' => $this->direccion,
            'descripcionRestaurante' => $this->descripcionRestaurante,
            'tipoServicioDelivery' => $this->tipoServicioDelivery,
            'metodosPago' => $this->metodosPago,
            'ratingPromedioEstrellas' => $this->ratingPromedioEstrellas,
            'imagenPortada' => $this->imagenPortada,
            'logotipoRestaurante' => $this->logotipoRestaurante,
            'estadoHabilitado' => $this->estadoHabilitado,
            'tipoDeContrato' => $this->tipoDeContrato,
        ];
    }
}
