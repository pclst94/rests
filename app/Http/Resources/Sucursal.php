<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Sucursal extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'idRestaurante' => $this->idRestaurante,
            'idDistrito' => $this->idDistrito,
            'nombreSucursal' => $this->nombreSucursal,
            'direccion' => $this->direccion,
            'telefono' => $this->telefono,
            'coordenadas' => $this->coordenadas,
        ];
    }
}
