<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductVariation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'idProductoExtra' => $this->idProductoExtra,
            'idVariacion' => $this->idVariacion,
            'extras' => $this->getExtras(),
            'precio' => $this->precio,
            'titulo' => $this->titulo,
            'sku' => $this->sku,
        ];
    }
}
