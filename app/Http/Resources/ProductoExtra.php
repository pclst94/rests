<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductoExtra extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'idProductoPadre' => $this->idProductoPadre,
            'idVariacion' => $this->idVariacion,
            'idHorario' => $this->idHorario,
            'titulo' => $this->titulo,
            'descripcion' => $this->descripcion,
            'variaciones' => $this->getVariations(),
            'extras' => $this->getAcompanamientos(),
            'idTipoDeProducto' => $this->idTipoDeProducto,
            'imagen' => $this->imagen,
            'horariosFechasApertura' => $this->horariosFechasApertura,
            'limiteExtra' => $this->limiteExtra,
            'isOfferOrProduct' => $this->isOfferOrProduct,
            'isExclusiveOffer' => $this->isExclusiveOffer,
            'idFechasHabilitacion' => $this->idFechasHabilitacion,
        ];
    }
}
