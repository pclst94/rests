<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TipoProducto extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'idRestaurante' => $this->idRestaurante,
            'nombre' => $this->nombre,
            'productos' => $this->getProductos(),
            'isExtra' => $this->isExtra,
        ];
    }
}
