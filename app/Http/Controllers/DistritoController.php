<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Distrito;
use App\Http\Resources\Distrito as DistritoResource;
use Illuminate\Support\Facades\Log;

class DistritoController extends Controller 
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $distritos = Distrito::where('status',1)->orderBy('id','DESC')->get();
      return DistritoResource::collection($distritos);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $distrito = Distrito::create([
      'idProvincia' => $request->idProvincia,
      'nombre' => $request->nombre
    ]);
    
    return new DistritoResource($distrito);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $distrito = Distrito::findOrFail($id);
    return new DistritoResource($distrito);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      $distrito = Distrito::findOrFail($id);
    $distrito->nombre = $request->nombre;
    if($distrito->save()){
      return new DistritoResource($distrito);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $distrito = Distrito::findorFail($id);
    $distrito->status = '0';
    if($distrito->save()){
      return new DistritoResource($distrito);
    }
  }
}

?>