<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoProducto;
use App\Http\Resources\TipoProducto as TipoProductoResource;

class TipoProductoController extends Controller 
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $tiprods = TipoProducto::where('status',1)->orderBy('id','DESC')->get();
    return TipoProductoResource::collection($tiprods);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->all();
    $tiprod = TipoProducto::create($input);
    
    return new TipoProductoResource($tiprod);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $tiprod = TipoProducto::find($id);
    return $tiprod ? new TipoProductoResource($tiprod) : [];
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $tiprod = TipoProducto::find($id);
    $input = $request->all();

    if($tiprod && $tiprod->update($input)){
      return new TipoProductoResource($tiprod);
    }
    return [];
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $tiprod = TipoProducto::findOrFail($id);
    $tiprod->status = '0';
    if($tiprod && $tiprod->save()){
      return new TipoProductoResource($tiprod);
    }
    return [];
  }
  
}

?>