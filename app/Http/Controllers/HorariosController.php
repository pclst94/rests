<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Horarios;
use App\Http\Resources\Horarios as HorariosResource;

class HorariosController extends Controller 
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $horarios = Horarios::where('status',1)->orderBy('id','DESC')->get();
      return HorariosResource::collection($horarios);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $horario = Horarios::create([
      'lunes' => $request->lunes,
      'martes' => $request->martes,
      'miercoles' => $request->miercoles,
      'jueves' => $request->jueves,
      'viernes' => $request->viernes,
      'sabado' => $request->sabado,
      'domingo' => $request->domingo,
      'feriados' => $request->feriados,
    ]);
    
    return new HorariosResource($horario);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $horario = Horarios::findOrFail($id);
    return new HorariosResource($horario);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $horario = Horarios::findOrFail($id);
    $input = $request->all();
    if($horario->update($input)){
      return new HorariosResource($horario);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $horario = Horarios::findorFail($id);
    $horario->status = '0';
    if($horario->save()){
      return new HorariosResource($horario);
    }
  }
  
}

?>