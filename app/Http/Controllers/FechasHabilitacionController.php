<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FechasHabilitacion;
use App\Http\Resources\FechasHabilitacion as FechasHabilitacionResource;

class FechasHabilitacionController extends Controller 
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $fechabs = FechasHabilitacion::where('status',1)->orderBy('id','DESC')->get();
    return FechasHabilitacionResource::collection($fechabs);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->all();
    $fechab = FechasHabilitacion::create($input);
    
    return new FechasHabilitacionResource($fechab);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $fechab = FechasHabilitacion::find($id);
    return $fechab ? new FechasHabilitacionResource($fechab) : [];
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $fechab = FechasHabilitacion::find($id);
    
    $input = $request->all();
    
    if($fechab && $fechab->update($input)){
      return new FechasHabilitacionResource($fechab);
    }
    return [];
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $fechab = FechasHabilitacion::findOrFail($id);
    $fechab->status = '0';
    if($fechab && $fechab->save()){
      return new FechasHabilitacionResource($fechab);
    }
    return [];
  }
  
}

?>