<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DiasFestivos;
use App\Http\Resources\DiasFestivos as DiasFestivosResource;

class DiasFestivosController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
      $diafests = DiasFestivos::where('status',1)->orderBy('id','DESC')->get();
      return DiasFestivosResource::collection($diafests);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $diafest = DiasFestivos::create([
      'fecha' => $request->fecha,
    ]);
    
    return new DiasFestivosResource($diafest);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $diafest = DiasFestivos::findOrFail($id);
    return new DiasFestivosResource($diafest);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $diafest = DiasFestivos::findOrFail($id);
    $input = $request->all();
    if($diafest->update($input)){
      return new DiasFestivosResource($diafest);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $diafest = DiasFestivos::findorFail($id);
    $diafest->status = '0';
    if($diafest->save()){
      return new DiasFestivosResource($diafest);
    }
  }
}
