<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ProductVariation;
use App\Http\Resources\ProductVariation as ProductVariationResource;

class ProductVariationController extends Controller 
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $prodvars = ProductVariation::where('status',1)->orderBy('id','DESC')->get();
    return ProductVariationResource::collection($prodvars);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->all();
    $prodvar = ProductVariation::create($input);
    
    return new ProductVariationResource($prodvar);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $prodvar = ProductVariation::find($id);
    return $prodvar ? new ProductVariationResource($prodvar) : [];
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $prodvar = ProductVariation::find($id);
    $input = $request->all();

    if($prodvar && $prodvar->update($input)){
      return new ProductVariationResource($prodvar);
    }
    return [];
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $prodvar = ProductVariation::findOrFail($id);
    $prodvar->status = '0';
    if($prodvar && $prodvar->save()){
      return new ProductVariationResource($prodvar);
    }
    return [];
  }  
}

?>