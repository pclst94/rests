<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoriaEspecialidad;
use App\Http\Resources\CategoriaEspecialidad as CategoriaEspecialidadResource;

class CategoriaEspecialidadController extends Controller 
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $catesps = CategoriaEspecialidad::where('status',1)->orderBy('id','DESC')->get();
      return CategoriaEspecialidadResource::collection($catesps);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $catesp = CategoriaEspecialidad::create([
      'idCatPadre' => $request->idCatPadre,
      'nombre' => $request->nombre
    ]);
    
    return new CategoriaEspecialidadResource($catesp);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $catesp = CategoriaEspecialidad::findOrFail($id);
    return new CategoriaEspecialidadResource($catesp);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $catesp = CategoriaEspecialidad::findOrFail($id);
    $input = $request->all();
    if($catesp->update($input)){
      return new CategoriaEspecialidadResource($catesp);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $catesp = CategoriaEspecialidad::findorFail($id);
    $catesp->status = '0';
    if($catesp->save()){
      return new CategoriaEspecialidadResource($catesp);
    }
  }
  
}

?>
