<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductoExtra;
use App\Http\Resources\ProductoExtra as ProductoExtraResource;

class ProductoExtraController extends Controller 
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $prodextras = ProductoExtra::where('status',1)->orderBy('id','DESC')->get();
    return ProductoExtraResource::collection($prodextras);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->all();
    $prodextra = ProductoExtra::create($input);
    
    return new ProductoExtraResource($prodextra);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $prodextra = ProductoExtra::find($id);
    return $prodextra ? new ProductoExtraResource($prodextra) : [];
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $prodextra = ProductoExtra::find($id);
    $input = $request->all();

    if($prodextra && $prodextra->update($input)){
      return new ProductoExtraResource($prodextra);
    }
    return [];
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $prodextra = ProductoExtra::findOrFail($id);
    $prodextra->status = '0';
    if($prodextra && $prodextra->save()){
      return new ProductoExtraResource($prodextra);
    }
    return [];
  }
  
}

?>