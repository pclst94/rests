<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provincia;
use App\Http\Resources\Provincia as ProvinciaResource;

class ProvinciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provincias = Provincia::where('status',1)->orderBy('id','DESC')->get();
        return ProvinciaResource::collection($provincias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provincia = Provincia::create([
            'nombre' => $request->nombre,
        ]);
        
        return new ProvinciaResource($provincia);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provincia = Provincia::findorFail($id);
        return new ProvinciaResource($provincia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $provincia = Provincia::findOrFail($id);
        //$provincia->nombre = $request->nombre;
        $provincia->nombre = $request->nombre;
        if($provincia->save()){
            return new ProvinciaResource($provincia);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provincia = Provincia::findOrFail($id);
        $provincia->status = '0';
        if($provincia->save()){
            return new ProvinciaResource($provincia);
        }
    }
}
