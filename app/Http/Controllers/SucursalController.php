<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sucursal;
use App\Http\Resources\Sucursal as SucursalResource;

class SucursalController extends Controller 
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $sucursals = Sucursal::where('status',1)->orderBy('id','DESC')->get();
    return SucursalResource::collection($sucursals);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->all();
    $sucursal = Sucursal::create($input);
    
    return new SucursalResource($sucursal);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $sucursal = Sucursal::find($id);
    return $sucursal ? new SucursalResource($sucursal) : [];
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $sucursal = Sucursal::find($id);
    $input = $request->all();

    if($sucursal && $sucursal->update($input)){
      return new SucursalResource($sucursal);
    }
    return [];
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $sucursal = Sucursal::findOrFail($id);
    $sucursal->status = '0';
    if($sucursal && $sucursal->save()){
      return new SucursalResource($sucursal);
    }
    return [];
  }
  
}

?>