<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurante;
use App\Horarios;
use App\Http\Resources\Restaurante as RestauranteResource;

use Illuminate\Support\Facades\Log;

class RestauranteController extends Controller 
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
    date_default_timezone_set('America/Bogota');
    $current_day = strftime("%A");
    $restaurantes = Restaurante::where('status',1)->orderBy('id','DESC')->get();
    $restaurantes_abiertos = collect([]);
    foreach ($restaurantes as $rest) {
      $service_hours = $rest->getHorario()->get();
      if(count($service_hours) > 0){
        $current_service_hours = $service_hours[0][$current_day];
        if (
          date('H:i') > substr($current_service_hours,0,strpos($current_service_hours,";")) &&
          date('H:i') < substr($current_service_hours,strpos($current_service_hours,";") + 1)
        ) {
          $restaurantes_abiertos = $restaurantes_abiertos->concat([$rest]);
        }
      }
    }
    return RestauranteResource::collection($restaurantes_abiertos);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $restaurante = Restaurante::create([
      'idHorario' => $request->idHorario,
      'razonSocial' => $request->razonSocial,
      'Ruc' => $request->Ruc,
      'Telefono' => $request->Telefono,
      'montoMinimoPedido' => $request->montoMinimoPedido,
      'ratioDeliveryCostoKilometro' => $request->ratioDeliveryCostoKilometro,
      'costoMinimoDelivery' => $request->costoMinimoDelivery,
      'direccion' => $request->direccion,
      'descripcionRestaurante' => $request->descripcionRestaurante,
      'tipoServicioDelivery' => $request->tipoServicioDelivery,
      'metodosPago' => $request->metodosPago,
      'ratingPromedioEstrellas' => $request->ratingPromedioEstrellas,
      'imagenPortada' => $request->imagenPortada,
      'logotipoRestaurante' => $request->logotipoRestaurante,
      'estadoHabilitado' => $request->estadoHabilitado,
    ]);
    
    return new RestauranteResource($restaurante);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $restaurante = Restaurante::find($id);
    return $restaurante ? new RestauranteResource($restaurante) : [];
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $restaurante = Restaurante::findOrFail($id);
    $input = $request->all();
    if($restaurante->update($input)){
      return new RestauranteResource($restaurante);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $restaurante = Restaurante::findorFail($id);
    $restaurante->status = '0';
    if($restaurante->save()){
      return new RestauranteResource($restaurante);
    }
  }
  
}

?>