<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductoExtraTable extends Migration {

	public function up()
	{
		Schema::create('productoExtra', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idProductoPadre')->unsigned()->nullable();
			$table->integer('idVariacion')->unsigned()->nullable();
			$table->integer('idHorario')->unsigned();
			$table->integer('idTipoDeProducto')->unsigned();
			$table->string('titulo')->nullable();
			$table->string('descripcion')->nullable();
			$table->longText('imagen')->nullable();
			$table->string('horariosFechasApertura')->nullable();
			$table->integer('limiteExtra')->nullable();
			$table->integer('isOfferOrProduct')->nullable();
			$table->boolean('isExclusiveOffer')->nullable();
			$table->timestamps();
			$table->integer('idFechasHabilitacion')->unsigned()->nullable();
			$table->boolean('status')->default(true);
		});
	}

	public function down()
	{
		Schema::drop('productoExtra');
	}
}