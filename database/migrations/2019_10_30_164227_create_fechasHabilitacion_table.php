<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFechasHabilitacionTable extends Migration {

	public function up()
	{
		Schema::create('fechasHabilitacion', function(Blueprint $table) {
			$table->increments('id');
			$table->date('fechaInicio');
			$table->date('fechaFinal');
			$table->boolean('status')->default(true);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('fechasHabilitacion');
	}
}