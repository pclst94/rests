<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoProductoTable extends Migration {

	public function up()
	{
		Schema::create('tipoProducto', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idRestaurante')->unsigned();
			$table->string('nombre');
			$table->timestamps();
			$table->boolean('isExtra');
			$table->boolean('status')->default(true);
		});
	}

	public function down()
	{
		Schema::drop('tipoProducto');
	}
}