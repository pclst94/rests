<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateComentariosRatingTable extends Migration {

	public function up()
	{
		Schema::create('ComentariosRating', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idRestaurante')->unsigned();
			$table->string('comentario');
			$table->integer('puntuacion');
			$table->boolean('status')->default(true);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('ComentariosRating');
	}
}