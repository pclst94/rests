<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProvinciaTable extends Migration {

	public function up()
	{
		Schema::create('provincia', function(Blueprint $table) {
			$table->increments('id', true);
			$table->string('nombre');
			$table->boolean('status')->default(true);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('provincia');
	}
}