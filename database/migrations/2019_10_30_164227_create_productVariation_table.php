<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductVariationTable extends Migration {

	public function up()
	{
		Schema::create('productVariation', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idProductoExtra')->unsigned();
			$table->integer('idVariacion')->unsigned(); // por ejemplo: la variacion de una gaseosa (gaseosa: productoxtra.id=4 y 
														// KR: variation.id=15, variation.idProductoExtra=4) le pertenece a una variation
														// de una salchipapa (salchipapa: productoxtra.id=8, salchipapa monster: variation.id=33, 
														// variation.idProductoExtra=8)
			$table->string('titulo');
			$table->double('precio');
			$table->string('sku')->nullable();
			$table->boolean('status')->default(true);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('productVariation');
	}
}