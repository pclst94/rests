<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriaEspecialidadTable extends Migration {

	public function up()
	{
		Schema::create('categoriaEspecialidad', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idCatPadre')->unsigned()->nullable();
			$table->string('nombre');
			$table->boolean('status')->default(true);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('categoriaEspecialidad');
	}
}