<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('restaurante', function(Blueprint $table) {
			$table->foreign('idHorario')->references('id')->on('horarios')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('sucursal', function(Blueprint $table) {
			$table->foreign('idRestaurante')->references('id')->on('restaurante')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('sucursal', function(Blueprint $table) {
			$table->foreign('idDistrito')->references('id')->on('distrito')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('distrito', function(Blueprint $table) {
			$table->foreign('idProvincia')->references('id')->on('provincia')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('categoriaEspecialidad', function(Blueprint $table) {
			$table->foreign('idCatPadre')->references('id')->on('categoriaEspecialidad')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('ComentariosRating', function(Blueprint $table) {
			$table->foreign('idRestaurante')->references('id')->on('restaurante')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('productoExtra', function(Blueprint $table) {
			$table->foreign('idProductoPadre')->references('id')->on('productoExtra')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('productoExtra', function(Blueprint $table) {
			$table->foreign('idHorario')->references('id')->on('horarios')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('productoExtra', function(Blueprint $table) {
			$table->foreign('idTipoDeProducto')->references('id')->on('tipoProducto')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('productoExtra', function(Blueprint $table) {
			$table->foreign('idFechasHabilitacion')->references('id')->on('fechasHabilitacion')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('productVariation', function(Blueprint $table) {
			$table->foreign('idProductoExtra')->references('id')->on('productoExtra')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('tipoProducto', function(Blueprint $table) {
			$table->foreign('idRestaurante')->references('id')->on('restaurante')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('horarios', function(Blueprint $table) {
			$table->foreign('idDiafestivo')->references('id')->on('diasFestivos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('restaurante', function(Blueprint $table) {
			$table->dropForeign('restaurante_idHorario_foreign');
		});
		Schema::table('sucursal', function(Blueprint $table) {
			$table->dropForeign('sucursal_idRestaurante_foreign');
		});
		Schema::table('sucursal', function(Blueprint $table) {
			$table->dropForeign('sucursal_idDistrito_foreign');
		});
		Schema::table('distrito', function(Blueprint $table) {
			$table->dropForeign('distrito_idProvincia_foreign');
		});
		Schema::table('categoriaEspecialidad', function(Blueprint $table) {
			$table->dropForeign('categoriaEspecialidad_idCatPadre_foreign');
		});
		Schema::table('ComentariosRating', function(Blueprint $table) {
			$table->dropForeign('ComentariosRating_idRestaurante_foreign');
		});
		Schema::table('productoExtra', function(Blueprint $table) {
			$table->dropForeign('productoExtra_idProductoPadre_foreign');
		});
		Schema::table('productoExtra', function(Blueprint $table) {
			$table->dropForeign('productoExtra_idHorario_foreign');
		});
		Schema::table('productoExtra', function(Blueprint $table) {
			$table->dropForeign('productoExtra_idTipoDeProducto_foreign');
		});
		Schema::table('productoExtra', function(Blueprint $table) {
			$table->dropForeign('productoExtra_idFechasHabilitacion_foreign');
		});
		Schema::table('productVariation', function(Blueprint $table) {
			$table->dropForeign('productVariation_idProductoExtra_foreign');
		});
		Schema::table('tipoProducto', function(Blueprint $table) {
			$table->dropForeign('tipoProducto_idRestaurante_foreign');
		});
		Schema::table('horarios', function(Blueprint $table) {
			$table->dropForeign('horarios_idDiafestivo_foreign');
		});
	}
}