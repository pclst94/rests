<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHorariosTable extends Migration {

	public function up()
	{
		Schema::create('horarios', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idDiafestivo')->unsigned()->nullable();
			$table->string('lunes')->nullable();
			$table->string('martes')->nullable();
			$table->string('miercoles')->nullable();
			$table->string('jueves')->nullable();
			$table->string('viernes')->nullable();
			$table->string('sabado')->nullable();
			$table->string('domingo')->nullable();
			$table->string('feriados')->nullable();
			$table->boolean('status')->default(true);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('horarios');
	}
}