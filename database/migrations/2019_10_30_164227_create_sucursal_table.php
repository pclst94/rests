<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSucursalTable extends Migration {

	public function up()
	{
		Schema::create('sucursal', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idRestaurante')->unsigned();
			$table->integer('idDistrito')->unsigned();
			$table->string('nombreSucursal')->nullable();
			$table->string('direccion')->nullable();
			$table->string('telefono')->nullable();
			$table->string('coordenadas')->nullable();
			$table->boolean('status')->default(true);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('sucursal');
	}
}