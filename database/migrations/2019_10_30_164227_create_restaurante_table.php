<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRestauranteTable extends Migration {

	public function up()
	{
		Schema::create('restaurante', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('idHorario')->unsigned()->nullable();
			$table->string('razonSocial')->nullable();
			$table->string('Ruc')->nullable();
			$table->string('Telefono')->nullable();
			$table->double('montoMinimoPedido')->nullable();
			$table->double('ratioDeliveryCostoKilometro')->nullable();
			$table->double('costoMinimoDelivery')->nullable();
			$table->string('direccion')->nullable();
			$table->string('descripcionRestaurante')->nullable();
			$table->integer('tipoServicioDelivery')->nullable();
			$table->string('metodosPago')->nullable();
			$table->double('ratingPromedioEstrellas')->nullable();
			$table->longText('imagenPortada')->nullable();
			$table->longText('logotipoRestaurante')->nullable();
			$table->boolean('estadoHabilitado')->nullable();
			$table->integer('tipoDeContrato')->nullable();
			$table->boolean('status')->default(true);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('restaurante');
	}
}