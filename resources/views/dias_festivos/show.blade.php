@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Dias Festivo' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('dias_festivos.dias_festivo.destroy', $diasFestivo->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('dias_festivos.dias_festivo.index') }}" class="btn btn-primary" title="Show All Dias Festivo">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('dias_festivos.dias_festivo.create') }}" class="btn btn-success" title="Create New Dias Festivo">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('dias_festivos.dias_festivo.edit', $diasFestivo->id ) }}" class="btn btn-primary" title="Edit Dias Festivo">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Dias Festivo" onclick="return confirm(&quot;Click Ok to delete Dias Festivo.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Fecha</dt>
            <dd>{{ $diasFestivo->fecha }}</dd>
            <dt>Status</dt>
            <dd>{{ ($diasFestivo->status) ? 'Yes' : 'No' }}</dd>
            <dt>Created At</dt>
            <dd>{{ $diasFestivo->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $diasFestivo->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection